FROM centos:7
MAINTAINER ivagen
RUN yum update -y && yum install -y epel-release && yum clean all
RUN rpm -ivh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
RUN yum update -y && yum install -y \
php70-php \
php70-php-cli \
php70-php-pear \
php70-php-http \
php70-php-uuid \
php70-php-pdo \
php70-php-pgsql \
php70-php-mbstring 

RUN yum install -y nano wget git unzip curl \
 && yum clean all

RUN ln -s /usr/bin/php70 /usr/local/bin/php
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

ENV TERM xterm

WORKDIR "/var/www"

CMD "/var/www/server"
