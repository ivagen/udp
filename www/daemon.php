<?php

require_once __DIR__.'/vendor/autoload.php';

$manager = new Ko\ProcessManager();
$manager->demonize();
$manager->setProcessTitle('I_am_a_master!');
$manager->onShutdown(
    function () use ($manager) {
        echo 'Catch sigterm.Quiting...'.PHP_EOL;
        exit();
    }
);

echo 'Execute `kill '.getmypid().'` from console to stop script'.PHP_EOL;
while (true) {
    $manager->dispatch();
}