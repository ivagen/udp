<?php

use Clue\React\Multicast\Factory;

require_once __DIR__.'/vendor/autoload.php';

$address = isset($argv[1]) ? $argv[1] : '239.255.255.250:1900';

$loop = React\EventLoop\Factory::create();
$factory = new Factory($loop);
$sender = $factory->createSender();

// do not wait for incoming messages
$sender->pause();

// send a simple message
$message = 'ping 123';
$sender->send($message, $address);

$loop->run();